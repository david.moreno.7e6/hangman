package com.example.hangman

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.hangman.databinding.ActivityMenuBinding
import com.example.hangman.databinding.ActivityResultBinding

class ResultScreen : AppCompatActivity() {
    lateinit var binding: ActivityResultBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportActionBar != null){
            supportActionBar?.hide()
        }
        binding= ActivityResultBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle: Bundle? = intent.extras
        val result= bundle?.getString("result")
        val attempts= bundle?.getInt("attempts")
        val word= bundle?.getString("word")
        val dificultad= bundle?.getString("dificultad")
        if (result == "Lose"){
            binding.resultTittle.text= "AHORCADO!"
            binding.resultText.text= "Vaya, has sido ahorcado... Vuelve a intentarlo si tienes valor. La palabra oculta era:"
            binding.word.text= word
            binding.word.visibility= View.VISIBLE
        }
        else {
            binding.resultTittle.text= "FELICIDADES!"
            when(attempts){
                0-> binding.resultText.text= "Eres todo un crack en Hangman! Te has librado de la horca sin fallar. Enhorabuena!"
                1-> binding.resultText.text= "Con tan solo $attempts fallo no habrás sentido ni la presion de la horca..."
                2-> binding.resultText.text= "Tendrás algunos rasguños en el cuello despues de $attempts fallos, pero nada grave."
                3-> binding.resultText.text= "Vaya, empezaba a verte mal ahí arriba despues de $attempts fallos, pero lo has conseguido."
                4-> binding.resultText.text= "Tenias la cabeza como un tomate ! Aunque no me extraña después de fallar $attempts veces..."
                5-> binding.resultText.text= "Lo tuyo es vivir al límite! Te daba por un cadaver al haber fallado $attempts veces, pero escapaste de la muerte !"
            }

        }

        binding.playAgain.setOnClickListener {
            val toPlay= Intent(this, GameScreen::class.java)
            toPlay.putExtra("dificultad", dificultad)
            startActivity(toPlay)
        }
        binding.menu.setOnClickListener {
            val toMenu= Intent(this, MenuScreen::class.java)
            startActivity(toMenu)
        }
    }
}