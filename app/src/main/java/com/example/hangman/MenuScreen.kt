package com.example.hangman

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.hangman.databinding.ActivityMenuBinding


class MenuScreen : AppCompatActivity() {

    lateinit var binding: ActivityMenuBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportActionBar != null){
            supportActionBar?.hide()
        }
        binding= ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.ayuda.setOnClickListener {
            val alertDialog: android.app.AlertDialog? = android.app.AlertDialog.Builder(this@MenuScreen).create()
            if (alertDialog != null) {
                alertDialog.setTitle("BIENVENIDOS A HANGMAN")
                alertDialog.setMessage("Hangman es un juego que consiste en adivinar la palabra oculta. \n\n PREPARACIÓN: \n\n Selecciona dificultad (Fácil, Media o Difícil). Variará el tamaño de la palabra a acertar. \n\n JUGUEMOS: \n\n Al clicar 'JUGAR' aparecerán lineas horizontales con espacio entre ellas, es la palabra a acertar. Debajo un teclado con cada letra, las cuales cambiaran de color si las has usado. Si aciertas, saldrá la letra en la posicion correcta. Por cada fallo, irá formandose la imagen de Hangman. Se te permiten 6 fallos para no acabar ahorcado... \n\n MUCHA SUERTE !")
                alertDialog.setButton(
                    AlertDialog.BUTTON_NEUTRAL, "OK",
                    DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
                alertDialog.show()
            }

        }
        binding.jugarButton.setOnClickListener {
            var dificultadSelector= binding.dificultad.selectedItem.toString()
            val toPlay= Intent(this, GameScreen::class.java)
            toPlay.putExtra("dificultad", dificultadSelector)
            val toast= Toast.makeText(applicationContext, "Selecciona una dificultad", Toast.LENGTH_SHORT)
            when(dificultadSelector){
                "Fácil" -> startActivity(toPlay)
                "Media" ->  startActivity(toPlay)
                "Difícil" -> startActivity(toPlay)
                "Dificultad" -> toast.show()
            }

        }



    }
}