package com.example.hangman

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.example.hangman.databinding.ActivityGameBinding

class GameScreen : AppCompatActivity(), View.OnClickListener {
    lateinit var binding: ActivityGameBinding
    val facil = listOf<String>("C E R O", "R E J A", "P E R A", "S A C O", "C O M A", "G A T O", "P U R O", "B U Z O", "B U H O", "S E D A")
    val media = listOf<String>("S A B E R", "C A L V O", "S O B R E", "D O T E S", "C O B R A", "F U E G O", "P A T I O", "C O S T A", "P I Z Z A", "F I E R A" )
    val dificil = listOf<String>( "P R E M I O", "C A R T O N", "P I E D R A", "S A R T E N", "P A S T E L", "P A R C H E", "M A R F I L", "J A R R O N", "H I E R B A", "C A V I A R")
    lateinit var word: String
    var attempts = 0
    var position = -1
    var correct = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportActionBar != null){
            supportActionBar?.hide()
        }
        binding = ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle: Bundle? = intent.extras
        val dificultadEscogida = bundle?.getString("dificultad")
        if (dificultadEscogida == "Fácil") {
            binding.palabra.text = "_ _ _ _"
            word = facil.random()
        } else if (dificultadEscogida == "Media") {
            binding.palabra.text = "_ _ _ _ _"
            word = media.random()
        } else if (dificultadEscogida == "Difícil") {
            binding.palabra.text = "_ _ _ _ _ _"
            word = dificil.random()
        }
        when (attempts) {
            1 -> binding.actionImage.setImageResource(R.drawable.hangmanstep2)
            2 -> binding.actionImage.setImageResource(R.drawable.hangmanstep3)
            3 -> binding.actionImage.setImageResource(R.drawable.hangmanstep4)
            4 -> binding.actionImage.setImageResource(R.drawable.hangmanstep5)
            5 -> binding.actionImage.setImageResource(R.drawable.hangmanstep6)
            6 -> binding.actionImage.setImageResource(R.drawable.hangmanstep7)
        }
        binding.buttonA.setOnClickListener(this)
        binding.buttonB.setOnClickListener(this)
        binding.buttonC.setOnClickListener(this)
        binding.buttonD.setOnClickListener(this)
        binding.buttonE.setOnClickListener(this)
        binding.buttonF.setOnClickListener(this)
        binding.buttonG.setOnClickListener(this)
        binding.buttonH.setOnClickListener(this)
        binding.buttonI.setOnClickListener(this)
        binding.buttonJ.setOnClickListener(this)
        binding.buttonK.setOnClickListener(this)
        binding.buttonL.setOnClickListener(this)
        binding.buttonM.setOnClickListener(this)
        binding.buttonN.setOnClickListener(this)
        binding.buttonEnye.setOnClickListener(this)
        binding.buttonO.setOnClickListener(this)
        binding.buttonP.setOnClickListener(this)
        binding.buttonQ.setOnClickListener(this)
        binding.buttonR.setOnClickListener(this)
        binding.buttonS.setOnClickListener(this)
        binding.buttonT.setOnClickListener(this)
        binding.buttonU.setOnClickListener(this)
        binding.buttonV.setOnClickListener(this)
        binding.buttonW.setOnClickListener(this)
        binding.buttonX.setOnClickListener(this)
        binding.buttonY.setOnClickListener(this)
        binding.buttonZ.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        var dificultadEscogida= ""
        when(word.length){
            7-> dificultadEscogida= "Fácil"
            9-> dificultadEscogida= "Media"
            11-> dificultadEscogida= "Difícil"
        }
        position=-1
        correct=false
        val button = p0 as Button
        button.visibility= View.INVISIBLE
        val toResult= Intent(this, ResultScreen::class.java)
        val letra = button.text.toString()
        var palabra = binding.palabra.text.toString()
        val palabraArray = palabra.toCharArray()
        for (i in word) {
            position++
            if (letra == i.toString()) {
                this
                palabraArray[position] = letra.first()
                binding.palabra.text = String(palabraArray)
                correct = true
            }
        }
        if (correct == false) {
            this
            attempts++
            binding.attempts.text = "FALLOS: $attempts"
            if ( attempts == 6){
                toResult.putExtra("result", "Lose")
                toResult.putExtra("word", word)
                toResult.putExtra("dificultad", dificultadEscogida)
                startActivity(toResult)
            }
        }
        else if (correct== true && word== binding.palabra.text){
            this
            toResult.putExtra("attempts", attempts)
            toResult.putExtra("result", "Win")
            toResult.putExtra("dificultad", dificultadEscogida)
            startActivity(toResult)
        }
        when (attempts) {
            1 -> binding.actionImage.setImageResource(R.drawable.hangmanstep2)
            2 -> binding.actionImage.setImageResource(R.drawable.hangmanstep3)
            3 -> binding.actionImage.setImageResource(R.drawable.hangmanstep4)
            4 -> binding.actionImage.setImageResource(R.drawable.hangmanstep5)
            5 -> binding.actionImage.setImageResource(R.drawable.hangmanstep6)
            6 -> binding.actionImage.setImageResource(R.drawable.hangmanstep7)
        }
        when (letra){
            "A"-> binding.buttonAUsed.visibility= View.VISIBLE
            "B"-> binding.buttonBUsed.visibility= View.VISIBLE
            "C"-> binding.buttonCUsed.visibility= View.VISIBLE
            "D"-> binding.buttonDUsed.visibility= View.VISIBLE
            "E"-> binding.buttonEUsed.visibility= View.VISIBLE
            "F"-> binding.buttonFUsed.visibility= View.VISIBLE
            "G"-> binding.buttonGUsed.visibility= View.VISIBLE
            "H"-> binding.buttonHUsed.visibility= View.VISIBLE
            "I"-> binding.buttonIUsed.visibility= View.VISIBLE
            "J"-> binding.buttonJUsed.visibility= View.VISIBLE
            "K"-> binding.buttonKUsed.visibility= View.VISIBLE
            "L"-> binding.buttonLUsed.visibility= View.VISIBLE
            "M"-> binding.buttonMUsed.visibility= View.VISIBLE
            "N"-> binding.buttonNUsed.visibility= View.VISIBLE
            "Ñ"-> binding.buttonEnyeUsed.visibility= View.VISIBLE
            "O"-> binding.buttonOUsed.visibility= View.VISIBLE
            "P"-> binding.buttonPUsed.visibility= View.VISIBLE
            "Q"-> binding.buttonQUsed.visibility= View.VISIBLE
            "R"-> binding.buttonRUsed.visibility= View.VISIBLE
            "S"-> binding.buttonSUsed.visibility= View.VISIBLE
            "T"-> binding.buttonTUsed.visibility= View.VISIBLE
            "U"-> binding.buttonUUsed.visibility= View.VISIBLE
            "V"-> binding.buttonVUsed.visibility= View.VISIBLE
            "W"-> binding.buttonWUsed.visibility= View.VISIBLE
            "X"-> binding.buttonXUsed.visibility= View.VISIBLE
            "Y"-> binding.buttonYUsed.visibility= View.VISIBLE
            "Z"-> binding.buttonZUsed.visibility= View.VISIBLE

        }

    }
}

