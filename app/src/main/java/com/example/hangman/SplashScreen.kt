package com.example.hangman

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.example.hangman.databinding.ActivitySplashScreenBinding

class SplashScreen : AppCompatActivity() {
    lateinit var binding: ActivitySplashScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportActionBar != null){
            supportActionBar?.hide()
        }
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val handler= Handler(Looper.getMainLooper())
        handler.postDelayed({
            val toMenu= Intent(this, MenuScreen::class.java)
            startActivity(toMenu)
            finish()
        }, 2000)


    }
}